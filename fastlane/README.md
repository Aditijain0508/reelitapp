fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

## iOS

### ios appcenterbuild

```sh
[bundle exec] fastlane ios appcenterbuild
```

This lane will create a ReelIt App build and upload to appcenter

### ios testCoverage

```sh
[bundle exec] fastlane ios testCoverage
```

This lane will scan the project and generates the xctest data

----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
