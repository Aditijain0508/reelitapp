//
//  CustomTabbarController.swift
//  Main
//
//  Created by Sharvan Kumar Kumawat on 02/08/22.
//

import UIKit
import Login
import Browse

class CustomTabbarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let browseModule = BrowseList()
        let browseViewController = browseModule.createBrowseListViewController()
        browseViewController.tabBarItem.image = UIImage(named: "Browse")
        let navgitaionControllerForBrowse = UINavigationController(rootViewController: browseViewController)
        browseViewController.tabBarItem.title = "Browse"

        viewControllers = [navgitaionControllerForBrowse]
        
        let module = UserModule()
        let loginViewController = module.createLoginViewController()
        let navigationController = UINavigationController(rootViewController: loginViewController)
        loginViewController.tabBarItem.image = UIImage(named: "User")
        loginViewController.tabBarItem.title = "User"
        var array = self.viewControllers
        array?.append(navigationController)
        self.viewControllers = array
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
